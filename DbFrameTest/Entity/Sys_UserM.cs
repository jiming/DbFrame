﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrameTest.Entity
{
    using DbFrame.Class;

    [Table("Sys_User")]
    public class Sys_UserM : BaseEntity
    {

        //User_ID, User_Name, User_LoginName, User_Pwd, User_Email, User_CreateTime

        public Sys_UserM()
        {
            this.AddIgnore(item => new { item.User_CreateTime });
        }

        [Field("ID", FieldType = typeof(Guid?), IsPrimaryKey = true)]
        public Guid? User_ID { get; set; }

        [Field("用户名称")]
        public string User_Name { get; set; }

        [CRepeat(ErrorMessage = "{name}不能重复")]
        [Field("登陆名")]
        public string User_LoginName { get; set; }

        [Field("登陆密码")]
        public string User_Pwd { get; set; }

        [Field("邮件")]
        public string User_Email { get; set; }

        [Field("创建时间")]
        public DateTime? User_CreateTime { get; set; }








    }
}
