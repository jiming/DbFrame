﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrameTest
{
    using DbFrame;
    using DbFrame.Class;

    using DbFrameTest.Entity;

    class Program
    {
        public static List<Sys_UserM> list = new List<Sys_UserM>();

        static void Main(string[] args)
        {
            System.Diagnostics.Stopwatch s = new System.Diagnostics.Stopwatch();
            s.Start();


            DbFrameConfig.ConnectionString = "Server=.;Database=AppDB;User ID=sa;Password=123456;";
            var li = new List<SQL>();
            var db = new DBContext();

            var query = db
                .Query<MemberM, Sys_UserM>((a, b) => new { a.Member_Name, a.Member_ID, b.User_Name, _ukid = a.Member_ID })
                .LeftJoin((a, b) => a.Member_UserID == b.User_ID, "b")
                .Where((a, b) => a.Member_Name.Contains("测试"))
                .ToList<MemberM>();

            var id = Guid.NewGuid();

            Program.list.Add(new Sys_UserM()
            {
                User_CreateTime = DateTime.Now,
                User_Email = "12312312@qq.com",
                User_ID = id,
                User_LoginName = "hzy",
                User_Name = "hzy_123",
                User_Pwd = "hzypwd"
            });

            var obj = list.Find(w => w.User_ID == id);

            obj.User_Pwd = "哈哈哈";


            //for (int i = 0; i < 10; i++)
            //{
            //    var id = db.Add<Sys_UserM>(new Sys_UserM()
            //    {
            //        User_Email = i + "test@qq.com",
            //        User_LoginName = i + "hzyNew",
            //        User_Name = i + "hzyTestNew",
            //        User_Pwd = i + "123"
            //    }, li);

            //    if (!db.Edit<Sys_UserM>(() => new Sys_UserM
            //     {
            //         User_Name = "修改过" + i + "hzyTestNew"
            //     }, w => w.User_ID == id.GetGuid(), li))
            //    {
            //        Console.WriteLine("错误！");
            //    }
            //}

            //db.Delete<Sys_UserM>(w => w.User_CreateTime > DateTime.Now.AddDays(-1), li);

            //if (!db.Commit(li))
            //{
            //    Console.WriteLine("事务提交失败！");
            //}

            //var _Sys_UserM = new Sys_UserM();
            //_Sys_UserM.User_Email = "1123@QQ.COM";
            //_Sys_UserM.User_LoginName = "admin";
            //_Sys_UserM.User_Name = "hzy";
            //_Sys_UserM.User_Pwd = "123";

            ////验证实体
            //if (db.CheckModel(_Sys_UserM))
            //{
            //    db.Add(_Sys_UserM);
            //}
            //else
            //{
            //    Console.WriteLine(db.ErrorMessge);
            //}

            var Count = 0;
            var Query = db
                .Query<Sys_UserM>((a) => new { a.User_LoginName, a.User_Name, a.User_Email, a.User_Pwd, a.User_CreateTime, a.User_ID })
                .Where((a) => a.User_LoginName == "user");

            var dt = db.FindPaging(Query.ToSQL(), 1, 20, out Count, Query.GetSqlParameters());//分页查询

            Console.WriteLine("总数：" + Count);

            Console.WriteLine(" 耗时：" + (s.ElapsedMilliseconds * 0.001) + " s");
            Console.ReadKey();

        }
    }
}
