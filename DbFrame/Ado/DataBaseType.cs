﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.Ado
{
    public enum DataBaseType
    {
        /// <summary>
        /// SqlServer
        /// </summary>
        SqlServer,

        /// <summary>
        /// MySql
        /// </summary>
        MySql,

        /// <summary>
        /// Oracle
        /// </summary>
        Oracle,

        /// <summary>
        /// Other
        /// </summary>
        Other,

    }



}
