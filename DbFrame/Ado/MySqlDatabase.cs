﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.Ado
{
    using System.Data;
    using Dapper;
    using DbFrame.Class;
    using MySql.Data;
    using MySql.Data.MySqlClient;

    /// <summary>
    /// Mysql 支持类
    /// </summary>
    public class MySqlDatabase
    {

        private string _ConnectionString { get; set; }

        public MySqlDatabase(string ConnectionString)
        {
            this._ConnectionString = ConnectionString;
        }

        public IDbConnection GetDbConnection()
        {
            return new MySqlConnection(this._ConnectionString);
        }

        /// <summary>
        /// 分页 查询
        /// </summary>
        /// <returns></returns>
        public DataTable FindPaging(string SqlStr, int Page, int PageSize, out int Total, object Param)
        {
            Total = this.GetDbConnection().QuerySingle<int>("Select Count(1) From (" + SqlStr + ") As t", Param);
            int Num = (Page - 1) * PageSize;
            SqlStr += " limit " + Num + "," + PageSize;
            return Mapping.IDataReaderToDataTable(this.GetDbConnection().ExecuteReader(SqlStr, Param));
        }

    }
}
