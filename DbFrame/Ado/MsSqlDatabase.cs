﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.Ado
{
    using System.Data;
    using System.Data.SqlClient;
    using Dapper;
    using DbFrame.Class;

    /// <summary>
    /// Sql Server 支持类
    /// </summary>
    public class MsSqlDatabase
    {
        private string _ConnectionString { get; set; }

        public MsSqlDatabase(string ConnectionString)
        {
            this._ConnectionString = ConnectionString;
        }

        public IDbConnection GetDbConnection()
        {
            return new SqlConnection(this._ConnectionString);
        }

        /// <summary>
        /// 分页 查询
        /// </summary>
        /// <returns></returns>
        public DataTable FindPaging(string SqlStr, int Page, int PageSize, out int Total, object _Param)
        {
            Total = 0;

            //解析参数
            var dic = new Dictionary<string, object>();
            if (_Param is Dictionary<string, object>)
            {
                dic = (Dictionary<string, object>)_Param;
            }
            else if (_Param is object)
            {
                dic = Mapping.ObjectToDictionary(_Param);
            }

            foreach (var item in dic)
            {
                SqlStr = SqlStr.Replace("@" + item.Key, item.Value == null ? null : "'" + item.Value + "' ");
            }

            var _DynamicParameters = new DynamicParameters();
            _DynamicParameters.Add("@SQL", SqlStr, DbType.String, ParameterDirection.Input);
            _DynamicParameters.Add("@PAGE", Page, DbType.Int32, ParameterDirection.Input);
            _DynamicParameters.Add("@PAGESIZE", PageSize, DbType.Int32, ParameterDirection.Input);
            _DynamicParameters.Add("@PAGECOUNT", 0, DbType.Int32, ParameterDirection.Output);
            _DynamicParameters.Add("@RECORDCOUNT", 0, DbType.Int32, ParameterDirection.Output);

            var _IDataReader = this.GetDbConnection().ExecuteReader("PROC_SPLITPAGE", _DynamicParameters, null, 30, CommandType.StoredProcedure);
            //将 IDataReader 对象转换为 DataSet 
            DataSet _DataSet = new DbFrame.Ado.DapperExtend.HZYDataSet();
            _DataSet.Load(_IDataReader, LoadOption.OverwriteChanges, null, new DataTable[] { });

            if (_DataSet.Tables.Count == 2)
            {
                Total = _DynamicParameters.Get<int>("@RECORDCOUNT");
                return _DataSet.Tables[1];
            }
            return new DataTable();
        }


    }
}
