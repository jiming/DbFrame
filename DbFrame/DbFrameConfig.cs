﻿using DbFrame.Ado;
namespace DbFrame
{
    public class DbFrameConfig
    {

        public DbFrameConfig()
        {
            DbFrameConfig._DataBaseType = DataBaseType.SqlServer;
        }

        public static string ConnectionString { get; set; }

        public static DataBaseType _DataBaseType { get; set; }


    }
}
