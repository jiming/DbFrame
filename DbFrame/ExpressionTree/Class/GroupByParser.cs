﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.ExpressionTree.Class
{
    using System.Linq.Expressions;
    using DbFrame.Class;
    using System.Collections;

    public class GroupByParser
    {


        public GroupByParser(LambdaExpression _LambdaExpression, StringBuilder Code, Dictionary<string, string> Alias, Dictionary<string, object> Param)
        {
            try
            {
                var body = (_LambdaExpression.Body as NewExpression);
                var values = body.Arguments;
                var member = body.Members;
                Code.Append(" GROUP BY ");
                var column = new List<string>();
                var list_member = member.ToList();
                foreach (MemberExpression item in values)
                {
                    var it = item as MemberExpression;
                    column.Add((it.Expression as ParameterExpression).Name + "." + it.Member.Name);
                }
                Code.Append(string.Join(",", column));
            }
            catch (Exception)
            {
                throw new Exception("GROUP BY 语法错误 请使用 new{f1,f2}");
            }

        }









    }
}
