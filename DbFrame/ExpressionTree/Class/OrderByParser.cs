﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.ExpressionTree.Class
{
    using System.Linq.Expressions;
    using DbFrame.Class;
    using System.Collections;

    public class OrderByParser
    {

        public OrderByParser(LambdaExpression _LambdaExpression, StringBuilder Code, Dictionary<string, string> Alias, Dictionary<string, object> Param)
        {
            try
            {
                var body = (_LambdaExpression.Body as NewExpression);
                var values = body.Arguments;
                var member = body.Members;
                Code.Append(" ORDER BY ");
                var column = new List<string>();
                var list_member = member.ToList();
                foreach (MemberExpression item in values)
                {
                    var it = item as MemberExpression;
                    //检查是否有别名
                    var DisplayName = list_member[values.IndexOf(item)].Name;

                    if (DisplayName == it.Member.Name)
                        column.Add((it.Expression as ParameterExpression).Name + "." + it.Member.Name);
                    else
                    {
                        if (DisplayName.ToLower() != "asc" && DisplayName.ToLower() != "desc")
                            throw new Exception("ORDER BY 语法错误 请使用 asc 或者 desc 关键字");
                        column.Add((it.Expression as ParameterExpression).Name + "." + it.Member.Name + " " + DisplayName);
                    }

                }
                Code.Append(string.Join(",", column));
            }
            catch (Exception)
            {
                throw new Exception("ORDER BY 语法错误 请使用 new{f1,desc=f2} 类似这种语法！");
            }

        }

























    }
}
