﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.ExpressionTree.Class
{
    using System.Linq.Expressions;
    using DbFrame.Class;
    using System.Collections;

    public class JoinTableParser
    {


        public JoinTableParser(LambdaExpression _LambdaExpression, StringBuilder Code, Dictionary<string, string> Alias, Dictionary<string, object> Param, string JoinStr, string JoinTabName)
        {
            if (!Alias.ContainsKey(JoinTabName)) throw new Exception("链接表 别名未找到！");

            var Lambda = _LambdaExpression;
            var body = (Lambda.Body as BinaryExpression);
            Code.Append(" " + JoinStr + " ");
            var ByName = JoinTabName;
            var TabName = Alias[ByName] + " AS " + ByName;
            Code.Append(" " + TabName + " ON ");

            var _ParserArgs = new ParserArgs();
            Parser.Where(_LambdaExpression, _ParserArgs);

            Code.Append(_ParserArgs.Builder);

            foreach (var item in _ParserArgs.SqlParameters)
            {
                Param.Add(item.Key, item.Value);
            }

        }









    }
}
