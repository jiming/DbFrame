﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.ExpressionTree
{
    using System.Linq.Expressions;
    using DbFrame.Class;

    public static class Parser
    {
        public static void Where(Expression _Expression, ParserArgs _ParserArgs)
        {
            new ExpressionParser().Where(_Expression, _ParserArgs);
        }

        public static void Select(LambdaExpression _LambdaExpression, StringBuilder Code, Dictionary<string, string> Alias, Dictionary<string, object> Param)
        {
            new ExpressionParser().Select(_LambdaExpression, Code, Alias, Param);
        }

        public static void JoinTable(LambdaExpression _LambdaExpression, StringBuilder Code, Dictionary<string, string> Alias, Dictionary<string, object> Param, string JoinStr, string JoinTabName)
        {
            new ExpressionParser().JoinTable(_LambdaExpression, Code, Alias, Param, JoinStr, JoinTabName);
        }

        public static void OrderBy(LambdaExpression _LambdaExpression, StringBuilder Code, Dictionary<string, string> Alias, Dictionary<string, object> Param)
        {
            new ExpressionParser().OrderBy(_LambdaExpression, Code, Alias, Param);
        }

        public static void GroupBy(LambdaExpression _LambdaExpression, StringBuilder Code, Dictionary<string, string> Alias, Dictionary<string, object> Param)
        {
            new ExpressionParser().GroupBy(_LambdaExpression, Code, Alias, Param);
        }

    }
}
