﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.ExpressionTree
{

    using System.Linq.Expressions;
    using DbFrame.Class;
    using DbFrame.ExpressionTree.Class;

    public class ExpressionParser : IExpressionParser
    {

        public void Where(Expression _Expression, ParserArgs _ParserArgs)
        {
            new WhereParser(_Expression, _ParserArgs);
        }

        public void Select(LambdaExpression _LambdaExpression, StringBuilder Code, Dictionary<string, string> Alias, Dictionary<string, object> Param)
        {
            new SelectParser(_LambdaExpression, Code, Alias, Param);
        }

        public void JoinTable(LambdaExpression _LambdaExpression, StringBuilder Code, Dictionary<string, string> Alias, Dictionary<string, object> Param, string JoinStr, string JoinTabName)
        {
            new JoinTableParser(_LambdaExpression, Code, Alias, Param, JoinStr, JoinTabName);
        }

        public void OrderBy(LambdaExpression _LambdaExpression, StringBuilder Code, Dictionary<string, string> Alias, Dictionary<string, object> Param)
        {
            new OrderByParser(_LambdaExpression, Code, Alias, Param);
        }

        public void GroupBy(LambdaExpression _LambdaExpression, StringBuilder Code, Dictionary<string, string> Alias, Dictionary<string, object> Param)
        {
            new GroupByParser(_LambdaExpression, Code, Alias, Param);
        }


    }








}
