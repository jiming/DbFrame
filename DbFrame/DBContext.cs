﻿
/*
    * 
    * 
    * 
    * 
    * DbFrame 数据访问框架
    * 
    * 作者：Hzy 开源地址：https://gitee.com/hao-zhi-ying/DbFrame
    * 
    * 
    * 
    * 
    */



























using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame
{
    using System.Linq.Expressions;
    using System.Data;
    using DbFrame.Class;
    using DbFrame.Ado;
    using DbFrame.SqlContext;
    using DbFrame.SqlContext.Interface;

    public class DBContext : IAdd, IEdit, IDelete, IFind
    {
        protected AddContext _AddContext;
        protected EditContext _EditContext;
        protected DeleteContext _DeleteContext;
        protected FindContext _FindContext;
        public DbHelper _DbHelper;
        public string ErrorMessge = string.Empty;
        private string _ConnectionString = string.Empty;
        private DataBaseType _DataBaseType = DataBaseType.SqlServer;

        /// <summary>
        /// 数据库操作对象
        /// </summary>
        /// <param name="ConnectionString"></param>
        /// <param name="DataBaseType"></param>
        public DBContext(string ConnectionString = null, DataBaseType DataBaseType = DataBaseType.Other)
        {
            if (string.IsNullOrEmpty(ConnectionString))
                this._ConnectionString = DbFrameConfig.ConnectionString;
            else
                this._ConnectionString = ConnectionString;

            if (DataBaseType == DataBaseType.Other)
            {
                DataBaseType = DbFrameConfig._DataBaseType;
            }

            this._AddContext = new AddContext(this._ConnectionString, DataBaseType);
            this._EditContext = new EditContext(this._ConnectionString, DataBaseType);
            this._DeleteContext = new DeleteContext(this._ConnectionString, DataBaseType);
            this._FindContext = new FindContext(this._ConnectionString, DataBaseType);
            this._DbHelper = new DbHelper(this._ConnectionString, DataBaseType);
            this._DataBaseType = DataBaseType;

        }

        /// <summary>
        /// 设置错误消息
        /// </summary>
        /// <param name="Error"></param>
        private void SetError(string Error)
        {
            ErrorMessge = string.Empty;
            ErrorMessge = Error.Replace("\r\n", "<br />");
            return;
        }

        /// <summary>
        /// Json 转换为 List <T>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Json"></param>
        /// <returns></returns>
        public List<T> JsonToList<T>(string Json)
        {
            T[] str = Mapping.DeserializeObject<T[]>(Json) as T[];
            return new List<T>(str);
        }


        /// <summary>
        /// 验证实体
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CheckModel<T>(T _Model) where T : BaseEntity, new()
        {
            var _CheckContext = new CheckContext<T>(this._ConnectionString, this._DataBaseType);
            if (!_CheckContext.Check(_Model))
            {
                SetError(_CheckContext.ErrorMessage);
                return false;
            }
            return true;
        }




        /********************************************************/
        /************************数据库操作
        /********************************************************/


        /// <summary>
        /// 提交事务
        /// </summary>
        /// <returns></returns>
        public bool Commit(List<SQL> li, Action<int, Class.SQL, IDbTransaction> _CallBack = null)
        {
            if (_DbHelper.Commit(li, _CallBack))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Model"></param>
        /// <returns></returns>
        public object Add<T>(T Model) where T : BaseEntity, new()
        {
            var key = _AddContext.Add<T>(Model);
            if (string.IsNullOrEmpty(key.GetString()))
            {
                SetError("操作失败");
                return null;
            }
            return key;
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Model"></param>
        /// <returns></returns>
        public object Add<T>(Expression<Func<T>> Model) where T : BaseEntity, new()
        {
            var key = _AddContext.Add<T>(Model);
            if (string.IsNullOrEmpty(key.GetString()))
            {
                SetError("操作失败");
                return null;
            }
            return key;
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Model"></param>
        /// <param name="li"></param>
        /// <returns></returns>
        public object Add<T>(T Model, List<SQL> li) where T : BaseEntity, new()
        {
            var key = _AddContext.Add<T>(Model, li);
            if (string.IsNullOrEmpty(key.GetString()))
            {
                SetError("操作失败");
                return null;
            }
            return key;
        }

        /// <summary>
        /// Insert
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Model"></param>
        /// <param name="li"></param>
        /// <returns></returns>
        public object Add<T>(Expression<Func<T>> Model, List<SQL> li) where T : BaseEntity, new()
        {
            var key = _AddContext.Add<T>(Model, li);
            if (string.IsNullOrEmpty(key.GetString()))
            {
                SetError("操作失败");
                return null;
            }
            return key;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Set"></param>
        /// <param name="Where"></param>
        /// <returns></returns>
        public bool Edit<T>(T Set, Expression<Func<T, bool>> Where) where T : BaseEntity, new()
        {
            if (_EditContext.Edit(Set, Where))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Set"></param>
        /// <param name="Where"></param>
        /// <returns></returns>
        public bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where) where T : BaseEntity, new()
        {
            if (_EditContext.Edit(Set, Where))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Set"></param>
        /// <param name="Where"></param>
        /// <param name="li"></param>
        /// <returns></returns>
        public bool Edit<T>(T Set, Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new()
        {
            if (_EditContext.Edit(Set, Where, li))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Set"></param>
        /// <param name="Where"></param>
        /// <param name="li"></param>
        /// <returns></returns>
        public bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new()
        {
            if (_EditContext.Edit(Set, Where, li))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// Update 根据ID
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Set"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool EditById<T>(T Set, object Id) where T : BaseEntity, new()
        {
            if (_EditContext.EditById(Set, Id))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// Update 根据ID
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Set"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool EditById<T>(Expression<Func<T>> Set, object Id) where T : BaseEntity, new()
        {
            if (_EditContext.EditById(Set, Id))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// Update 根据ID
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Set"></param>
        /// <param name="Id"></param>
        /// <param name="li"></param>
        /// <returns></returns>
        public bool EditById<T>(T Set, object Id, List<SQL> li) where T : BaseEntity, new()
        {
            if (_EditContext.EditById(Set, Id, li))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// Update 根据ID
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Set"></param>
        /// <param name="Id"></param>
        /// <param name="li"></param>
        /// <returns></returns>
        public bool EditById<T>(Expression<Func<T>> Set, object Id, List<SQL> li) where T : BaseEntity, new()
        {
            if (_EditContext.EditById(Set, Id, li))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Where"></param>
        /// <returns></returns>
        public bool Delete<T>(Expression<Func<T, bool>> Where) where T : BaseEntity, new()
        {
            if (_DeleteContext.Delete<T>(Where))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Where"></param>
        /// <param name="li"></param>
        /// <returns></returns>
        public bool Delete<T>(Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new()
        {
            if (_DeleteContext.Delete<T>(Where, li))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// Delete 根据ID
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteById<T>(object Id) where T : BaseEntity, new()
        {
            if (_DeleteContext.DeleteById<T>(Id))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// Delete 根据ID
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Id"></param>
        /// <param name="li"></param>
        /// <returns></returns>
        public bool DeleteById<T>(object Id, List<SQL> li) where T : BaseEntity, new()
        {
            if (_DeleteContext.DeleteById<T>(Id, li))
                return true;
            SetError("操作失败");
            return false;
        }

        /// <summary>
        /// 查询得到 DataTable
        /// </summary>
        /// <param name="SqlStr"></param>
        /// <param name="Param"></param>
        /// <returns></returns>
        public DataTable FindTableBySql(string SqlStr, object Param = null)
        {
            return _FindContext.FindTableBySql(SqlStr, Param);
        }

        /// <summary>
        /// 得到单行单列数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="SqlStr"></param>
        /// <param name="Param"></param>
        /// <returns></returns>
        public T FindSingleBySql<T>(string SqlStr, object Param = null)
        {
            return _FindContext.FindSingleBySql<T>(SqlStr, Param);
        }

        /// <summary>
        /// 得到 实体 对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="SqlStr"></param>
        /// <param name="Param"></param>
        /// <returns></returns>
        public T FindBySql<T>(string SqlStr, object Param = null) where T : BaseEntity, new()
        {
            return _FindContext.FindBySql<T>(SqlStr, Param);
        }

        /// <summary>
        /// 得到 IEnumerable 集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="SqlStr"></param>
        /// <param name="Param"></param>
        /// <returns></returns>
        public IEnumerable<T> FindListBySql<T>(string SqlStr, object Param = null)
        {
            return _FindContext.FindListBySql<T>(SqlStr, Param);
        }

        /// <summary>
        /// 分页查询
        /// </summary>
        /// <param name="Sql"></param>
        /// <param name="Page"></param>
        /// <param name="PageSize"></param>
        /// <param name="Total"></param>
        /// <param name="Param"></param>
        /// <returns></returns>
        public DataTable FindPaging(string Sql, int Page, int PageSize, out int Total, object Param = null)
        {
            return _FindContext.FindPaging(Sql, Page, PageSize, out Total, Param);
        }

        /// <summary>
        /// 得到最大编号
        /// </summary>
        /// <param name="TabName"></param>
        /// <param name="FieldNum"></param>
        /// <param name="Where"></param>
        /// <param name="Param"></param>
        /// <returns></returns>
        public int FindMaxNumber(string TabName, string FieldNum, string Where = "", object Param = null)
        {
            return _FindContext.FindMaxNumber(TabName, FieldNum, Where, Param);
        }

        /// <summary>
        /// 得到 DataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Where"></param>
        /// <param name="OrderBy"></param>
        /// <returns></returns>
        public DataTable FindTable<T>(Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy) where T : BaseEntity, new()
        {
            return _FindContext.FindTable<T>(Where, OrderBy);
        }

        /// <summary>
        /// 得到 实体对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Where"></param>
        /// <returns></returns>
        public T Find<T>(Expression<Func<T, bool>> Where) where T : BaseEntity, new()
        {
            return _FindContext.Find<T>(Where);
        }

        /// <summary>
        /// 得到 IEnumerable 集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Where"></param>
        /// <param name="OrderBy"></param>
        /// <returns></returns>
        public IEnumerable<T> FindList<T>(Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy) where T : BaseEntity, new()
        {
            return _FindContext.FindList<T>(Where, OrderBy);
        }

        /// <summary>
        /// 得到 DataTable 可自定义 列
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Select"></param>
        /// <param name="Where"></param>
        /// <param name="OrderBy"></param>
        /// <returns></returns>
        public DataTable FindTable<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy) where T : BaseEntity, new()
        {
            return _FindContext.FindTable<T>(Select, Where, OrderBy);
        }

        /// <summary>
        /// 得到单行单列 列通过表达式树设置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="W"></typeparam>
        /// <param name="Select"></param>
        /// <param name="Where"></param>
        /// <returns></returns>
        public T FindSingle<T, W>(Expression<Func<W, object>> Select, Expression<Func<W, bool>> Where)
            where T : BaseEntity
            where W : BaseEntity, new()
        {
            return _FindContext.FindSingle<T, W>(Select, Where);
        }

        /// <summary>
        /// 得到单行单列 列通过字符串设置
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="W"></typeparam>
        /// <param name="Select"></param>
        /// <param name="Where"></param>
        /// <returns></returns>
        public T FindSingle<T, W>(string Select, Expression<Func<W, bool>> Where)
            where T : BaseEntity
            where W : BaseEntity, new()
        {
            return _FindContext.FindSingle<T, W>(Select, Where);
        }

        /// <summary>
        /// 得到实体对象 可自定义列
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Select"></param>
        /// <param name="Where"></param>
        /// <returns></returns>
        public T Find<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where) where T : BaseEntity, new()
        {
            return _FindContext.Find<T>(Select, Where);
        }

        /// <summary>
        /// 得到 IEnumerable 集合 可自定义列
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Select"></param>
        /// <param name="Where"></param>
        /// <param name="OrderBy"></param>
        /// <returns></returns>
        public IEnumerable<T> FindList<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy) where T : BaseEntity, new()
        {
            return _FindContext.FindList<T>(Select, Where, OrderBy);
        }


        public IQuery<T1> Query<T1>(Expression<Func<T1, object>> Select)
          where T1 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2> Query<T1, T2>(Expression<Func<T1, T2, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3> Query<T1, T2, T3>(Expression<Func<T1, T2, T3, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3, T4> Query<T1, T2, T3, T4>(Expression<Func<T1, T2, T3, T4, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity
            where T4 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3, T4, T5> Query<T1, T2, T3, T4, T5>(Expression<Func<T1, T2, T3, T4, T5, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity
            where T4 : BaseEntity
            where T5 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3, T4, T5, T6> Query<T1, T2, T3, T4, T5, T6>(Expression<Func<T1, T2, T3, T4, T5, T6, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity
            where T4 : BaseEntity
            where T5 : BaseEntity
            where T6 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3, T4, T5, T6, T7> Query<T1, T2, T3, T4, T5, T6, T7>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity
            where T4 : BaseEntity
            where T5 : BaseEntity
            where T6 : BaseEntity
            where T7 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3, T4, T5, T6, T7, T8> Query<T1, T2, T3, T4, T5, T6, T7, T8>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity
            where T4 : BaseEntity
            where T5 : BaseEntity
            where T6 : BaseEntity
            where T7 : BaseEntity
            where T8 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9> Query<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity
            where T4 : BaseEntity
            where T5 : BaseEntity
            where T6 : BaseEntity
            where T7 : BaseEntity
            where T8 : BaseEntity
            where T9 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> Query<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity
            where T4 : BaseEntity
            where T5 : BaseEntity
            where T6 : BaseEntity
            where T7 : BaseEntity
            where T8 : BaseEntity
            where T9 : BaseEntity
            where T10 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> Query<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity
            where T4 : BaseEntity
            where T5 : BaseEntity
            where T6 : BaseEntity
            where T7 : BaseEntity
            where T8 : BaseEntity
            where T9 : BaseEntity
            where T10 : BaseEntity
            where T11 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> Query<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity
            where T4 : BaseEntity
            where T5 : BaseEntity
            where T6 : BaseEntity
            where T7 : BaseEntity
            where T8 : BaseEntity
            where T9 : BaseEntity
            where T10 : BaseEntity
            where T11 : BaseEntity
            where T12 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> Query<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity
            where T4 : BaseEntity
            where T5 : BaseEntity
            where T6 : BaseEntity
            where T7 : BaseEntity
            where T8 : BaseEntity
            where T9 : BaseEntity
            where T10 : BaseEntity
            where T11 : BaseEntity
            where T12 : BaseEntity
            where T13 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> Query<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity
            where T4 : BaseEntity
            where T5 : BaseEntity
            where T6 : BaseEntity
            where T7 : BaseEntity
            where T8 : BaseEntity
            where T9 : BaseEntity
            where T10 : BaseEntity
            where T11 : BaseEntity
            where T12 : BaseEntity
            where T13 : BaseEntity
            where T14 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }

        public IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> Query<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, object>> Select)
            where T1 : BaseEntity
            where T2 : BaseEntity
            where T3 : BaseEntity
            where T4 : BaseEntity
            where T5 : BaseEntity
            where T6 : BaseEntity
            where T7 : BaseEntity
            where T8 : BaseEntity
            where T9 : BaseEntity
            where T10 : BaseEntity
            where T11 : BaseEntity
            where T12 : BaseEntity
            where T13 : BaseEntity
            where T14 : BaseEntity
            where T15 : BaseEntity, new()
        {
            return _FindContext.Query(Select);
        }



    }
}
