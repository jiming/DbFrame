﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.Class
{
    /// <summary>
    /// 字段描述
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class FieldAttribute : Attribute
    {
        public string Alias = string.Empty;
        /// <summary>
        /// 是否主键
        /// </summary>
        public bool IsPrimaryKey = false;
        /// <summary>
        /// 是否自增
        /// </summary>
        public bool IsIdentity = false;
        /// <summary>
        /// 属性类型
        /// </summary>
        public Type FieldType;
        /// <summary>
        /// 字段名称
        /// </summary>
        public string FieldName = string.Empty;
        /// <summary>
        /// 主键值
        /// </summary>
        public object Value = string.Empty;
        /// <summary>
        /// 字段描述
        /// </summary>
        /// <param name="_Alias">别名</param>
        public FieldAttribute(string _Alias)
        {
            this.Alias = _Alias;
        }
    }

}
