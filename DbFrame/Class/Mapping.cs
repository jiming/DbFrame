﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using System.Data;
using Newtonsoft.Json;

namespace DbFrame.Class
{
    public static class Mapping
    {

        /// <summary>
        /// 将对象序列化为Json字符串
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string SerializeObject<T>(this T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }

        /// <summary>
        /// 将 Json 字符串转换为 指定的对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T DeserializeObject<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }

        /// <summary>
        /// 将 Json 字符串转换为 object对象
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static object DeserializeObject(string json)
        {
            return JsonConvert.DeserializeObject(json);
        }

        /// <summary>
        /// 将 DataRow 转换为 Entity 实体对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dr"></param>
        /// <returns></returns>
        public static T DataRowToEntity<T>(DataRow dr) where T : class, new()
        {
            var _Entity = ReflexHelper.CreateInstance<T>();
            var list = _Entity.GetType().GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);

            if (list.Length == 0) throw new Exception("找不到任何 公共属性！");

            foreach (var item in list)
            {
                string AttrName = item.Name;
                foreach (DataColumn dc in dr.Table.Columns)
                {
                    if (AttrName != dc.ColumnName) continue;
                    if (dr[dc.ColumnName] != DBNull.Value) item.SetValue(_Entity, dr[dc.ColumnName], null);
                }
            }
            return _Entity;
        }

        /// <summary>
        /// 将 datatable 转换为 list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <returns></returns>
        public static List<T> TableToList<T>(DataTable table) where T : class, new()
        {
            var list = new List<T>();

            var _Entity = ReflexHelper.CreateInstance<T>();
            var propertyInfo = _Entity.GetType().GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public);
            foreach (DataRow dr in table.Rows)
            {
                _Entity = ReflexHelper.CreateInstance<T>();
                foreach (var item in propertyInfo)
                {
                    string AttrName = item.Name;
                    foreach (DataColumn dc in dr.Table.Columns)
                    {
                        if (AttrName != dc.ColumnName) continue;
                        if (dr[dc.ColumnName] != DBNull.Value)
                        {
                            item.SetValue(_Entity, dr[dc.ColumnName], null);
                        }
                        else
                            item.SetValue(_Entity, null, null);
                    }
                }
                list.Add(_Entity);
            }
            return list;
        }

        /// <summary>
        /// 将 datatable 转换为 List<Dictionary<string,object>>
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static List<Dictionary<string, object>> TableToList(DataTable table)
        {
            var list = new List<Dictionary<string, object>>();
            var dic = new Dictionary<string, object>();
            foreach (DataRow dr in table.Rows)
            {
                if (dic != null) dic = new Dictionary<string, object>();
                foreach (DataColumn dc in table.Columns)
                {
                    if (dc.DataType == typeof(DateTime))
                    {
                        dic.Add(dc.ColumnName, dr[dc.ColumnName].GetDateTimeString("yyyy-MM-dd HH:mm:ss"));
                    }
                    else
                    {
                        dic.Add(dc.ColumnName, dr[dc.ColumnName]);
                    }
                }
                list.Add(dic);
            }
            return list;
        }

        /// <summary>
        /// 将IDataReader 转换为 DataTable
        /// </summary>
        /// <param name="_IDataReader"></param>
        /// <returns></returns>
        public static DataTable IDataReaderToDataTable(IDataReader _IDataReader)
        {
            DataTable dt = new DataTable();
            dt.Load(_IDataReader);
            return dt;
        }

        /// <summary>
        /// 将匿名对象转换为字典
        /// </summary>
        /// <param name="Attribute"></param>
        /// <returns></returns>
        public static Dictionary<string, object> ObjectToDictionary(object Attribute)
        {
            var di = new Dictionary<string, object>();

            Type ty = Attribute.GetType();

            var fields = ty.GetProperties().ToList();

            foreach (var item in fields)
            {
                di.Add(item.Name, item.GetValue(Attribute).ToString());
            }

            return di;
        }





    }
}
