﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.Class
{
    using System.Linq.Expressions;

    public class BaseEntity
    {
        public BaseEntity() { }

        /// <summary>
        /// 存储不会操作数据的字段
        /// </summary>
        private List<string> Ignore = new List<string>();

        /// <summary>
        /// 添加忽略
        /// </summary>
        /// <param name="Field"></param>
        public void AddIgnore(string Field)
        {
            Ignore.Add(Field);
        }

        /// <summary>
        /// 获取忽略
        /// </summary>
        /// <returns></returns>
        public List<string> GetIgnore()
        {
            return this.Ignore;
        }


    }
}
