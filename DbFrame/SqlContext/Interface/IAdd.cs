﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.SqlContext.Interface
{
    using DbFrame.Class;
    using System.Linq.Expressions;

    public interface IAdd
    {

        object Add<T>(T Model) where T : BaseEntity, new();
        object Add<T>(Expression<Func<T>> Model) where T : BaseEntity, new();
        object Add<T>(T Model, List<SQL> li) where T : BaseEntity, new();
        object Add<T>(Expression<Func<T>> Model, List<SQL> li) where T : BaseEntity, new();




    }
}
