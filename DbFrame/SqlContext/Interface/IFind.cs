﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.SqlContext.Interface
{
    using System.Data;
    using System.Linq.Expressions;
    using DbFrame;
    using DbFrame.Class;

    public interface IFind
    {
        /************************* 基础 函数***************************/
        DataTable FindTableBySql(string SqlStr, object Param);

        T FindSingleBySql<T>(string SqlStr, object Param);

        T FindBySql<T>(string SqlStr, object Param) where T : BaseEntity, new();

        IEnumerable<T> FindListBySql<T>(string SqlStr, object Param);

        DataTable FindPaging(string Sql, int Page, int PageSize, out int Total, object Param);

        int FindMaxNumber(string TabName, string FieldNum, string Where, object Param);





        /************************* 表达式树 函数***************************/
        DataTable FindTable<T>(Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy) where T : BaseEntity, new();

        T Find<T>(Expression<Func<T, bool>> Where) where T : BaseEntity, new();

        IEnumerable<T> FindList<T>(Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy) where T : BaseEntity, new();



        /************************* 表达式树 函数 自定义 Select ***************************/
        DataTable FindTable<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy) where T : BaseEntity, new();

        T FindSingle<T, W>(Expression<Func<W, object>> Select, Expression<Func<W, bool>> Where)
            where T : BaseEntity
            where W : BaseEntity, new();

        T FindSingle<T, W>(string Select, Expression<Func<W, bool>> Where)
            where T : BaseEntity
            where W : BaseEntity, new();

        T Find<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where) where T : BaseEntity, new();

        IEnumerable<T> FindList<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy) where T : BaseEntity, new();




        /************************* 表达式树 函数 多表查询 ***************************/
        //IQuery<T1> Find<T1>(Expression<Func<T1, object>> Select)
        //    where T1 : BaseEntity, new();

        //IQuery<T1, T2> Find<T1, T2>(Expression<Func<T1, T2, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity, new();

        //IQuery<T1, T2, T3> Find<T1, T2, T3>(Expression<Func<T1, T2, T3, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity, new();

        //IQuery<T1, T2, T3, T4> Find<T1, T2, T3, T4>(Expression<Func<T1, T2, T3, T4, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity
        //    where T4 : BaseEntity, new();

        //IQuery<T1, T2, T3, T4, T5> Find<T1, T2, T3, T4, T5>(Expression<Func<T1, T2, T3, T4, T5, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity
        //    where T4 : BaseEntity
        //    where T5 : BaseEntity, new();

        //IQuery<T1, T2, T3, T4, T5, T6> Find<T1, T2, T3, T4, T5, T6>(Expression<Func<T1, T2, T3, T4, T5, T6, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity
        //    where T4 : BaseEntity
        //    where T5 : BaseEntity
        //    where T6 : BaseEntity, new();

        //IQuery<T1, T2, T3, T4, T5, T6, T7> Find<T1, T2, T3, T4, T5, T6, T7>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity
        //    where T4 : BaseEntity
        //    where T5 : BaseEntity
        //    where T6 : BaseEntity
        //    where T7 : BaseEntity, new();

        //IQuery<T1, T2, T3, T4, T5, T6, T7, T8> Find<T1, T2, T3, T4, T5, T6, T7, T8>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity
        //    where T4 : BaseEntity
        //    where T5 : BaseEntity
        //    where T6 : BaseEntity
        //    where T7 : BaseEntity
        //    where T8 : BaseEntity, new();

        //IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9> Find<T1, T2, T3, T4, T5, T6, T7, T8, T9>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity
        //    where T4 : BaseEntity
        //    where T5 : BaseEntity
        //    where T6 : BaseEntity
        //    where T7 : BaseEntity
        //    where T8 : BaseEntity
        //    where T9 : BaseEntity, new();

        //IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> Find<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity
        //    where T4 : BaseEntity
        //    where T5 : BaseEntity
        //    where T6 : BaseEntity
        //    where T7 : BaseEntity
        //    where T8 : BaseEntity
        //    where T9 : BaseEntity
        //    where T10 : BaseEntity, new();

        //IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> Find<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity
        //    where T4 : BaseEntity
        //    where T5 : BaseEntity
        //    where T6 : BaseEntity
        //    where T7 : BaseEntity
        //    where T8 : BaseEntity
        //    where T9 : BaseEntity
        //    where T10 : BaseEntity
        //    where T11 : BaseEntity, new();

        //IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> Find<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity
        //    where T4 : BaseEntity
        //    where T5 : BaseEntity
        //    where T6 : BaseEntity
        //    where T7 : BaseEntity
        //    where T8 : BaseEntity
        //    where T9 : BaseEntity
        //    where T10 : BaseEntity
        //    where T11 : BaseEntity
        //    where T12 : BaseEntity, new();

        //IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> Find<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity
        //    where T4 : BaseEntity
        //    where T5 : BaseEntity
        //    where T6 : BaseEntity
        //    where T7 : BaseEntity
        //    where T8 : BaseEntity
        //    where T9 : BaseEntity
        //    where T10 : BaseEntity
        //    where T11 : BaseEntity
        //    where T12 : BaseEntity
        //    where T13 : BaseEntity, new();

        //IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> Find<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity
        //    where T4 : BaseEntity
        //    where T5 : BaseEntity
        //    where T6 : BaseEntity
        //    where T7 : BaseEntity
        //    where T8 : BaseEntity
        //    where T9 : BaseEntity
        //    where T10 : BaseEntity
        //    where T11 : BaseEntity
        //    where T12 : BaseEntity
        //    where T13 : BaseEntity
        //    where T14 : BaseEntity, new();

        //IQuery<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> Find<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(Expression<Func<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, object>> Select)
        //    where T1 : BaseEntity
        //    where T2 : BaseEntity
        //    where T3 : BaseEntity
        //    where T4 : BaseEntity
        //    where T5 : BaseEntity
        //    where T6 : BaseEntity
        //    where T7 : BaseEntity
        //    where T8 : BaseEntity
        //    where T9 : BaseEntity
        //    where T10 : BaseEntity
        //    where T11 : BaseEntity
        //    where T12 : BaseEntity
        //    where T13 : BaseEntity
        //    where T14 : BaseEntity
        //    where T15 : BaseEntity, new();




    }
}
