﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.SqlContext.Interface
{
    //
    using DbFrame.Class;
    using System.Linq.Expressions;

    public interface IDelete
    {

        bool Delete<T>(Expression<Func<T, bool>> Where) where T : BaseEntity, new();
        bool Delete<T>(Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new();




        bool DeleteById<T>(object Id) where T : BaseEntity, new();
        bool DeleteById<T>(object Id, List<SQL> li) where T : BaseEntity, new();

    }
}
