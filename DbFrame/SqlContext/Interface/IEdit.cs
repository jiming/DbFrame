﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.SqlContext.Interface
{
    //
    using DbFrame.Class;
    using System.Linq.Expressions;

    public interface IEdit
    {
        bool Edit<T>(T Set, Expression<Func<T, bool>> Where) where T : BaseEntity, new();
        bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where) where T : BaseEntity, new();
        bool Edit<T>(T Set, Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new();
        bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new();





        bool EditById<T>(T Set, object Id) where T : BaseEntity, new();
        bool EditById<T>(Expression<Func<T>> Set, object Id) where T : BaseEntity, new();
        bool EditById<T>(T Set, object Id, List<SQL> li) where T : BaseEntity, new();
        bool EditById<T>(Expression<Func<T>> Set, object Id, List<SQL> li) where T : BaseEntity, new();




    }
}
