﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.SqlContext
{
    //
    using System.Linq.Expressions;
    using DbFrame.Ado;
    using DbFrame.Class;
    using ExpressionTree;

    public class EditContext : Abstract.AbstractEdit
    {

        private string _ConnectionString { get; set; }
        private DbHelper _DbHelper = null;
        private DataBaseType _DataBaseType;
        public EditContext(string ConnectionString, DataBaseType DataBaseType)
        {
            this._ConnectionString = ConnectionString;
            this._DbHelper = new DbHelper(ConnectionString, DataBaseType);
            this._DataBaseType = DataBaseType;
        }





        public override bool Edit<T>(T Set, Expression<Func<T, bool>> Where)
        {
            var list = new List<MemberBinding>();
            var fileds = ReflexHelper.GetPropertyInfos(typeof(T)).Where(w => w.Name != Set.GetKey().FieldName);
            foreach (var item in fileds)
            {
                //检测有无忽略字段
                if (Set.GetIgnore().Find(f => f == item.Name) != null) continue;
                list.Add(Expression.Bind(item, Expression.Constant(item.GetValue(Set), item.PropertyType)));
            }

            return Execute<T>(Expression.MemberInit(Expression.New(typeof(T)), list), Where);
        }

        public override bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where)
        {
            return Execute<T>(Set.Body as MemberInitExpression, Where);
        }

        public override bool Edit<T>(T Set, Expression<Func<T, bool>> Where, List<SQL> li)
        {
            var list = new List<MemberBinding>();
            var fileds = ReflexHelper.GetPropertyInfos(typeof(T)).Where(w => w.Name != Set.GetKey().FieldName);
            foreach (var item in fileds)
            {
                //检测有无忽略字段
                if (Set.GetIgnore().Find(f => f == item.Name) != null) continue;
                list.Add(Expression.Bind(item, Expression.Constant(item.GetValue(Set), item.PropertyType)));
            }

            return Execute<T>(Expression.MemberInit(Expression.New(typeof(T)), list), Where, li);
        }

        public override bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where, List<SQL> li)
        {
            return Execute<T>(Set.Body as MemberInitExpression, Where, li);
        }



        public override bool EditById<T>(T Set, object Id)
        {
            var list = new List<MemberBinding>();
            var fileds = ReflexHelper.GetPropertyInfos(typeof(T)).Where(w => w.Name != Set.GetKey().FieldName);
            foreach (var item in fileds)
            {
                //检测有无忽略字段
                if (Set.GetIgnore().Find(f => f == item.Name) != null) continue;
                list.Add(Expression.Bind(item, Expression.Constant(item.GetValue(Set), item.PropertyType)));
            }

            return ExecuteById<T>(Expression.MemberInit(Expression.New(typeof(T)), list), Id);
        }

        public override bool EditById<T>(Expression<Func<T>> Set, object Id)
        {
            return ExecuteById<T>((Set.Body as MemberInitExpression), Id);
        }

        public override bool EditById<T>(T Set, object Id, List<SQL> li)
        {
            var list = new List<MemberBinding>();
            var fileds = ReflexHelper.GetPropertyInfos(typeof(T)).Where(w => w.Name != Set.GetKey().FieldName);
            foreach (var item in fileds)
            {
                //检测有无忽略字段
                if (Set.GetIgnore().Find(f => f == item.Name) != null) continue;
                list.Add(Expression.Bind(item, Expression.Constant(item.GetValue(Set), item.PropertyType)));
            }

            return ExecuteById<T>(Expression.MemberInit(Expression.New(typeof(T)), list), Id, li);
        }

        public override bool EditById<T>(Expression<Func<T>> Set, object Id, List<SQL> li)
        {
            return ExecuteById<T>(Set.Body as MemberInitExpression, Id, li);
        }




        private bool Execute<T>(MemberInitExpression Set, Expression<Func<T, bool>> Where, List<SQL> li = null) where T : BaseEntity, new()
        {
            var sql = this.SqlString(Set, Where);
            if (li == null)
            {
                if (!_DbHelper.Commit(new List<SQL>() { sql })) return false;
            }
            else
            {
                li.Add(sql);
            }
            return true;
        }

        private bool ExecuteById<T>(MemberInitExpression Set, object Id, List<SQL> li = null) where T : BaseEntity, new()
        {
            var sql = this.SqlStringById<T>(Set, Id);
            if (li == null)
            {
                if (!_DbHelper.Commit(new List<SQL>() { sql })) return false;
            }
            else
            {
                li.Add(sql);
            }
            return true;
        }

        private SQL Analysis<T>(MemberInitExpression Set, Action<ParserArgs, T> CallBack) where T : BaseEntity, new()
        {
            Code = new StringBuilder();

            var Model = ReflexHelper.CreateInstance<T>();
            string TabName = Model.GetTabelName();
            var set = new List<string>();

            Code.Append("UPDATE " + TabName + " SET ");

            //获取 Where 语句
            var pa = new ParserArgs();
            pa.TabIsAlias = false;

            CallBack(pa, Model);

            var _Where = pa.Builder.GetString();

            foreach (MemberAssignment item in Set.Bindings)
            {
                //检测有无忽略字段
                if (Model.GetIgnore().Find(f => f == item.Member.Name) != null) continue;
                var value = this.Eval(item.Expression);
                var name = item.Member.Name;
                var len = pa.SqlParameters.Count;

                set.Add(name + "=@" + name + "_" + len);
                pa.SqlParameters.Add(name + "_" + len, value);
            }

            Code.Append(string.Join(",", set) + " WHERE 1=1 " + _Where + ";");

            return new SQL(Code.ToString(), pa.SqlParameters);
        }

        private SQL SqlString<T>(MemberInitExpression Set, Expression<Func<T, bool>> Where) where T : BaseEntity, new()
        {
            return Analysis<T>(Set, (_ParserArgs, Model) =>
            {
                if (Where != null)
                {
                    _ParserArgs.Builder.Append("AND ");
                    Parser.Where(Where, _ParserArgs);
                }
            });
        }

        private SQL SqlStringById<T>(MemberInitExpression Set, object Id) where T : BaseEntity, new()
        {
            return Analysis<T>(Set, (_ParserArgs, Model) =>
            {
                var _Key = Model.GetKey(); if (_Key == null) throw new ArgumentNullException("找不到 实体 中的 主键！");
                _ParserArgs.Builder.Append(" AND " + _Key.FieldName + "=@" + _Key.FieldName + "");
                _ParserArgs.SqlParameters.Add("@" + _Key.FieldName, Id);
            });
        }










    }
}
