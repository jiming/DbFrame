﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.SqlContext.Abstract
{
    using System.Data;
    using System.Linq.Expressions;
    using DbFrame.SqlContext.Interface;
    using DbFrame.Class;
    using DbFrame.Ado;

    public abstract class AbstractFind : FindBaseClass, IFind
    {


        public AbstractFind(string ConnectionString, DataBaseType DataBaseType) : base(ConnectionString, DataBaseType) { }


        /************************* 基础 函数***************************/
        public abstract DataTable FindTableBySql(string SqlStr, object Param);

        public abstract T FindSingleBySql<T>(string SqlStr, object Param);

        public abstract T FindBySql<T>(string SqlStr, object Param) where T : BaseEntity, new();

        public abstract IEnumerable<T> FindListBySql<T>(string SqlStr, object Param);

        public abstract DataTable FindPaging(string Sql, int Page, int PageSize, out int Total, object Param);

        public abstract int FindMaxNumber(string TabName, string FieldNum, string Where, object Param);




        /************************* 表达式树 函数***************************/
        public abstract DataTable FindTable<T>(Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy)
            where T : BaseEntity, new();

        public abstract T Find<T>(Expression<Func<T, bool>> Where)
            where T : BaseEntity, new();

        public abstract IEnumerable<T> FindList<T>(Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy)
            where T : BaseEntity, new();




        /************************* 表达式树 函数 自定义 Select ***************************/
        public abstract DataTable FindTable<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy)
            where T : BaseEntity, new();

        public abstract T FindSingle<T, W>(Expression<Func<W, object>> Select, Expression<Func<W, bool>> Where)
            where T : BaseEntity
            where W : BaseEntity, new();

        public abstract T FindSingle<T, W>(string Select, Expression<Func<W, bool>> Where)
            where T : BaseEntity
            where W : BaseEntity, new();

        public abstract T Find<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where)
            where T : BaseEntity, new();

        public abstract IEnumerable<T> FindList<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy)
            where T : BaseEntity, new();





        /************************* 基础 函数***************************/
        DataTable IFind.FindTableBySql(string SqlStr, object Param)
        {
            return this.FindTableBySql(SqlStr, Param);
        }

        T IFind.FindSingleBySql<T>(string SqlStr, object Param)
        {
            return this.FindSingleBySql<T>(SqlStr, Param);
        }

        T IFind.FindBySql<T>(string SqlStr, object Param)
        {
            return this.FindBySql<T>(SqlStr, Param);
        }

        IEnumerable<T> IFind.FindListBySql<T>(string SqlStr, object Param)
        {
            return this.FindListBySql<T>(SqlStr, Param);
        }

        DataTable IFind.FindPaging(string Sql, int Page, int PageSize, out int Total, object Param)
        {
            return this.FindPaging(Sql, Page, PageSize, out Total, Param);
        }

        int IFind.FindMaxNumber(string TabName, string FieldNum, string Where, object Param)
        {
            return this.FindMaxNumber(TabName, FieldNum, Where, Param);
        }



        /************************* 表达式树 函数***************************/
        DataTable IFind.FindTable<T>(Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy)
        {
            return this.FindTable<T>(Where, OrderBy);
        }

        T IFind.Find<T>(Expression<Func<T, bool>> Where)
        {
            return this.Find<T>(Where);
        }

        IEnumerable<T> IFind.FindList<T>(Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy)
        {
            return this.FindList<T>(Where, OrderBy);
        }



        /************************* 表达式树 函数 自定义 Select ***************************/
        DataTable IFind.FindTable<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy)
        {
            return this.FindTable<T>(Select, Where, OrderBy);
        }

        T IFind.FindSingle<T, W>(Expression<Func<W, object>> Select, Expression<Func<W, bool>> Where)
        {
            return this.FindSingle<T, W>(Select, Where);
        }

        T IFind.FindSingle<T, W>(string Select, Expression<Func<W, bool>> Where)
        {
            return this.FindSingle<T, W>(Select, Where);
        }

        T IFind.Find<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where)
        {
            return this.Find<T>(Select, Where);
        }

        IEnumerable<T> IFind.FindList<T>(Expression<Func<T, object>> Select, Expression<Func<T, bool>> Where, Expression<Func<T, object>> OrderBy)
        {
            return this.FindList<T>(Select, Where, OrderBy);
        }







    }
}
