﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.SqlContext.Abstract
{
    //
    using DbFrame.SqlContext.Interface;
    using DbFrame.Class;
    using System.Linq.Expressions;

    public abstract class AbstractEdit : BaseCalss, IEdit
    {

        public abstract bool Edit<T>(T Set, Expression<Func<T, bool>> Where) where T : BaseEntity, new();
        public abstract bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where) where T : BaseEntity, new();
        public abstract bool EditById<T>(T Set, object Id) where T : BaseEntity, new();
        public abstract bool EditById<T>(Expression<Func<T>> Set, object Id) where T : BaseEntity, new();


        public abstract bool Edit<T>(T Set, Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new();
        public abstract bool Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new();
        public abstract bool EditById<T>(T Set, object Id, List<SQL> li) where T : BaseEntity, new();
        public abstract bool EditById<T>(Expression<Func<T>> Set, object Id, List<SQL> li) where T : BaseEntity, new();



        bool IEdit.Edit<T>(T Set, Expression<Func<T, bool>> Where)
        {
            return Edit<T>(Set, Where);
        }

        bool IEdit.Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where)
        {
            return Edit<T>(Set, Where);
        }

        bool IEdit.EditById<T>(T Set, object Id)
        {
            return EditById<T>(Set, Id);
        }

        bool IEdit.EditById<T>(Expression<Func<T>> Set, object Id)
        {
            return EditById<T>(Set, Id);
        }


        bool IEdit.Edit<T>(T Set, Expression<Func<T, bool>> Where, List<Class.SQL> li)
        {
            return Edit<T>(Set, Where, li);
        }

        bool IEdit.Edit<T>(Expression<Func<T>> Set, Expression<Func<T, bool>> Where, List<Class.SQL> li)
        {
            return Edit<T>(Set, Where, li);
        }
        bool IEdit.EditById<T>(T Set, object Id, List<Class.SQL> li)
        {
            return EditById<T>(Set, Id, li);
        }

        bool IEdit.EditById<T>(Expression<Func<T>> Set, object Id, List<Class.SQL> li)
        {
            return EditById<T>(Set, Id, li);
        }

    }
}
