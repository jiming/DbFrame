﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.SqlContext.Abstract
{
    //
    using DbFrame.SqlContext.Interface;
    using DbFrame.Class;
    using System.Linq.Expressions;

    public abstract class AbstractDelete : BaseCalss, IDelete
    {

        public abstract bool Delete<T>(Expression<Func<T, bool>> Where) where T : BaseEntity, new();
        public abstract bool Delete<T>(Expression<Func<T, bool>> Where, List<SQL> li) where T : BaseEntity, new();

        public abstract bool DeleteById<T>(object Id) where T : BaseEntity, new();
        public abstract bool DeleteById<T>(object Id, List<SQL> li) where T : BaseEntity, new();

        bool IDelete.Delete<T>(Expression<Func<T, bool>> Where)
        {
            return Delete<T>(Where);
        }

        bool IDelete.Delete<T>(Expression<Func<T, bool>> Where, List<Class.SQL> li)
        {
            return Delete<T>(Where, li);
        }


        bool IDelete.DeleteById<T>(object Id)
        {
            return DeleteById<T>(Id);
        }

        bool IDelete.DeleteById<T>(object Id, List<Class.SQL> li)
        {
            return DeleteById<T>(Id, li);
        }


    }
}
