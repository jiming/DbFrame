﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.SqlContext.Abstract
{
    using System.Linq.Expressions;
    public class BaseCalss
    {
        protected StringBuilder Code = new StringBuilder();


        protected object Eval(Expression _Expression)
        {
            var cast = Expression.Convert(_Expression, typeof(object));
            return Expression.Lambda<Func<object>>(cast).Compile().Invoke();
        }



    }
}
