﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbFrame.SqlContext.Abstract
{
    //
    using DbFrame.SqlContext.Interface;
    using DbFrame.Class;
    using System.Linq.Expressions;

    public abstract class AbstractAdd : BaseCalss, IAdd
    {
        public abstract object Add<T>(T Model) where T : BaseEntity, new();
        public abstract object Add<T>(Expression<Func<T>> Func) where T : BaseEntity, new();
        public abstract object Add<T>(T Model, List<SQL> li) where T : BaseEntity, new();
        public abstract object Add<T>(Expression<Func<T>> Func, List<SQL> li) where T : BaseEntity, new();

        object IAdd.Add<T>(T Model)
        {
            return Add<T>(Model);
        }

        object IAdd.Add<T>(Expression<Func<T>> Func)
        {
            return Add<T>(Func);
        }

        object IAdd.Add<T>(T Model, List<Class.SQL> li)
        {
            return Add<T>(Model, li);
        }

        object IAdd.Add<T>(Expression<Func<T>> Func, List<Class.SQL> li)
        {
            return Add<T>(Func, li);
        }



    }
}
